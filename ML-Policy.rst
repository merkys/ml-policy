ML-Policy: Machine Learning & Software Freedom
==============================================
:Author: Copyright (C) 2019-2020 Mo Zhou ``<lumin@debian.org>``, CC-BY-SA-4.0
:Date: Apr. 2020

Preface
-------

**Version:** 0.2.5 (experimental)

Existing practice of machine learning and deep learning challenged our
understanding and interpretation of software freedom. This document aims to
address the following issues: (1) taxonomy of machine learning related files;
(2) definition of reproducibility for related files; (3) policy terms that
should be considered by the Debian project and any other rigorous free software
distributors.

Software freedom is the core concern of this document: According to FSF[1],
Roughly, it means that the freedom for the users to run, copy, distribute,
study, change and improve the software.

**Disclaimer:** This draft is UNOFFICIAL and does not represent the point of view of the Debian Project.

Abbreviations
^^^^^^^^^^^^^

* FOSS: Free and Open Source Software

Taxonomy for Machine Learning Related Files
-------------------------------------------

Generally, there are three types of files related to machine learning software:
*Human-Readable Source*, *Serialized Model*, and automatically *Generated
Artifacts*.

+ **Human-Readable Source** involves: source code (e.g. written in C++, Python)
  for model creation, modification (training, fine-tuning), inference, and
  analysis; data (materials such as digits, texts, audios, images, videos) used
  for training, validating, and testing the machine learning model (applicable
  for supervised/unsupervised learning); environment simulators (e.g. Atari
  game simulator) used for training the agent model (applicable for
  reinforcement learning).

+ **Serialized Model** refers to a machine learning model serialized in any
  format (e.g. binary protobuf, json, python pickle, etc).

+ **Generated Artifacts** refers to objects produced from machine learning
  models, with or without external inputs as hints. For instance, an
  auto-generated article/image/song/video is deemed as an automatically
  generated artifact from the model.

Human-Readable Source
^^^^^^^^^^^^^^^^^^^^^

Following the principles of software freedom and existing practice, the
**Human-Readable Sources** are easy to be classified.

1. **Free Human-Readable Source**: Materials published under a FOSS license and are DFSG-compatible.

2. **Non-Free Human-Readable Source**: Materials not released under a FOSS license or incompatible with DFSG.

Serialized Model
^^^^^^^^^^^^^^^^

In terms of software freedom and information security, the pre-trained machine
learning models can be divided into the following *non-overlapping* categories:

1. **Free Model** is a model satisfying *ALL* the following conditions:
   (1) FOSS-Licensed & DFSG-compliant;
   (2) trained from explicitly FOSS-licensed & DFSG-compliant datasets (e.g. for supervised or unsupervised learning) or simulators (e.g. for reinforcement learning), and the dataset is publicly available to anonymous users;
   (3) corresponding training program is present and complete;

2. **ToxicCandy Model** is a model satisfying *ALL* the following conditions:
   (1) FOSS-Licensed & DFSG-compliant;
   (2) trained from unknown, private, or non-free datasets or simulators;
   (3) corresponding training program or script is present and complete;

3. **NonFree Model** is a model satisfying *ANY* of the following conditions:
   (1) non-free licensed or doesn't comply with DFSG;
   (2) corresponding training program or script is absent or incomplete;
   (3) unusable without non-free software (including but not limited to drivers
       and libraries);
   (4) cannot be categorized as anyone other type;

Generated Artifacts
^^^^^^^^^^^^^^^^^^^

Artifacts produced by the machine learning models and optional raw data, such
as images, videos, audios and fonts generated by the models, should
automatically inherit the property of the model itself and the original raw
data if applicable.

1. **Free Artifact** is an artifact produced by **Free Model**, and it must
   satisfy all the following conditions:
   (1) The original raw data, if there is any, used for generating the artifact is FOSS-Licensed & DFSG-compliant;

2. **NonFree Artifact** is and artifact produced by **ToxicCandy Model** or
   **NonFree Model**. Artifacts that cannot be categorised into other types
   should be deemed as **NonFree Artifact**.

Reproducibility Definition for Machine Learning Related Files
-------------------------------------------------------------

We define two types of reproducibility for Serialized Model: Type-F(unction)
and Type-H(ash).

1. **Type-F Reproducible**: When trying to reproduce the model from scratch
   with the same experimental settings, the model retains the same pattern for
   the training curves (including loss curves and validation performance
   curves), and retains comparable final performance metrics on the validation
   dataset. A reasonable error in floating point numbers is acceptable.
   Hashsum mismatch is acceptable.

2. **Type-H Reproducible**: **Type-F Reproducibility** plus identical file
   hashsums.

Reproducibility of Generative Artifacts: TODO

ML-Policy
---------

1. [``Archive-Sections``] "Free Model" + "Type-F reproducibility" can enter the
   main section. "Free Model" without "Type-F reproducibility" is highly
   questionable and cannot enter the main section.  "ToxicCandy Model" and
   "NonFree Model" should enter the non-free section if they have to enter our
   archive.

2. [``Dependency``] A package that includes a pre-trained machine learning model,
   must also include the corresponding training program, or ``Suggests:`` the
   package that provides the corresponding training program.

3. [``Whitelist``] "ToxicCandy" models in the whitelist, such as some of those
   used in input methods can enter the main section. Modification of the
   whitelist should be publicly discussed.

4. [``Tainting-Combination``] When Human-Readable Sources or Serialized Models
   from different sources are used in conjunction, non-free components taint
   the combination as a whole. For example, the combination of "Free" and
   "ToxicCandy" is "ToxicCandy"; and the combination of "Free" & "NonFree" is
   "NonFree".

5. [``Reproduce-Rules``] The maintainer is suggested to maintain a
   ``reproduce-model`` target in ``debian/rules`` for reproducing the
   pre-trained model, even if actually reproducing the model has a cost.

6. [``Tainting-Pipeline``] In a pipeline of training (creating) model or
   producing artifacts with models, nonfree-ness of any involved Human-Readable
   Source taints the freeness of Serialized Model; Nonfree-ness of any
   Serialized Model and input data (if any) taints the freeness of Generated
   Artifacts;

7. [``External-Data``] *Free Models* trained using large datasets (e.g. Wikipedia
   dump) can be uploaded to the archive without uploading the training data
   first, as long as the following conditions are satisfied: (1) it is commonly
   believed that uploading this large dataset to our archive may be problematic;
   (2) the dataset is always publicly and anonymously available through network;
   (3) the package maintainer should maintain a ``get-external-data`` target
   in ``debian/rules``. Besides, when using external data (data outside of
   our archive), the strength of ``Reproduce-Rules`` policy term should be
   changed from "suggested" to "highly reocmmended".

ToxicCandy Whitelist
^^^^^^^^^^^^^^^^^^^^

1. Models used in traditional input methods. (Excluding RNN models, e.g. LSTM)

Acknowledgements
----------------

Many thanks to people who provided feedbacks when drafting this document.

References
----------

1. What is free software? https://www.gnu.org/philosophy/free-sw.html
